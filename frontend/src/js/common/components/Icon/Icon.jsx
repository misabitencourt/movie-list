import React, { PureComponent } from 'react';
import iconSet from './colletion'; 

class Icon extends PureComponent {
  render() {
    return (
      <img style={{display: 'inline-block'}}
           src={`data:image/svg+xml;base64,${btoa(iconSet[this.props.icon])}`} />
    );
  }
}

export default Icon;
