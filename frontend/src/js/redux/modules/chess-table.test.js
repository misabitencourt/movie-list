import {actions, reducers} from './chess-table';
import knightMoves from '../../../../__mocks__/knight-moves';

describe('Chess table action and reducers', () => {
    beforeEach(() => fetch.resetMocks());

    it('Chess table actions and reducers definitions', () => {
        expect(!!  actions.selectKnightPosition).toEqual(true);
        expect(!! reducers.SELECT_KNIGHT_POS).toEqual(true);
    });

    it('Knight position preview action', async () => {
        fetch.mockResponseOnce(knightMoves);
        const actionResult = await actions.selectKnightPosition({position: 'a3'}).payload();
        expect(!! actionResult.movesPreview).toEqual(true);
        expect(actionResult.movesPreview.length).toEqual(23);
        const resultsExpect = ['a3','c3','d2','a3','a7','c3','c7','d4','d6','a1',
                               'a3','b4','d4','e1','e3','a3','a5','b2','b6','d2','d6','e3','e5'];
        for (let preview of actionResult.movesPreview) {
            expect(resultsExpect.indexOf(preview.pos)).not.toEqual(-1);
            expect(preview.steps).toEqual(2);
        }
        const reduced = reducers.SELECT_KNIGHT_POS({}, {payload: actionResult});
        expect(reduced.selectedKnightPosition).toEqual(actionResult.selectedKnightPosition);
        expect(reduced.movesPreview).toEqual(actionResult.movesPreview);
    });

    it('Moves API error handler', async () => {
        fetch.resetMocks();
        fetch.mockReject(new Error('fake error message'));
        expect(actions.selectKnightPosition({position: 'a2'}).payload()).resolves.toThrow();
    });
});