<?php

namespace App\Repositories;

use App\Models\ApiCache as ApiCache;
use App\Exceptions\NotFoundException;
use App\Exceptions\InvalidException;

class ApiCacheRepository extends BaseRepository
{
    protected $searchable = [
        'key',
    ];

    function __construct()
    {
        parent::__construct(new ApiCache);
    }

    public function list(string $search = null): array
    {
        return [];
    }

    public function create(array $resource): array
    {
        $newRes = parent::create($resource);
        
        return $newRes;
    }

    public function update(int $id, array $data): array
    {
        return null;
    }

    public function find(int $id): array
    {
        $resource = parent::find($id);
        
        return $resource;
    }
}
