<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiCache extends Model
{
    protected $fillable = [
        'key',
        'value',
        'created_at',
    ];
}
