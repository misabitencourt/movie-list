<?php

namespace App\Services;

use App\Repositories\ApiCacheRepository as ApiCacheRepository;
use App\Repositories\User as UserRepository;

class MovieService
{
    private $repo;

    function __construct()
    {
        $this->repo = new ApiCacheRepository();
    }

    public function list($search): array
    {
        return [];
    }

    public function destroy(int $id)
    {
        $this->repo->destroy($id);
    }

    public function create(array $resource): array
    {
        return [];
    }

    public function update(int $id, array $data): array
    {
        return [];
    }

    public function find(int $id): array
    {
        $user = $this->userRepository->find($id);
        return $user;
    }

}
